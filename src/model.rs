use rocket::serde::{Deserialize, Serialize, json::Json};
use std::sync::Mutex;

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Flight {
    pub tenant: String,
    pub departure: Airports,
    pub arrival: Airports,
    pub internal_code: String,
    pub available_options: Vec<FlightOption>,
    pub stop_overs: Vec<Airports>,
    pub total_seats: i32,
    pub price: i32
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub enum Airports {
    DTW,
    CDG,
    JFK,
    ORD,
    EWR,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct FlightOption {
    pub code: String,
    pub name: String,
    pub price: String
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Ticket{
    pub flight_code: String,
    pub person: Person,
    pub price: i32,
    pub booking_date: String,
    pub option_codes: Vec<String>
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Person{
    pub first_name: String,
    pub last_name: String,
    pub birth_date: String
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Booking{
    pub total_price: i32,
    pub currency: Currency,
    pub tickets: Vec<Ticket>,
    pub api_url: Option<String>
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct FlightProvider{
    pub code: String,
    pub api_key: String
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Currency{
    pub currency_code: String,
    pub currency_name: String
}
#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub enum ErrorInsert {
    TechnicalError,
}

pub struct SharedData {
    pub flights: Mutex<Vec<Flight>>,
    pub providers: Mutex<Vec<FlightProvider>>,
    pub bookings: Mutex<Vec<Booking>>
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct FlightRequest {
    pub provider_key: String,
    pub flights: Vec<Flight>
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct BookingRequest {
    pub provider_key: String,
    pub booking: Booking
}