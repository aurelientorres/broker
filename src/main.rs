#[macro_use] extern crate rocket;
use rocket::serde::{Deserialize, Serialize, json::Json};
use rocket::State;
use std::sync::Mutex;
mod model;
use model::{Flight, FlightOption, Airports, SharedData, FlightProvider, FlightRequest, BookingRequest};



#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[get("/flights")]
fn get_all_flights(shared: &State<SharedData>) -> String {
    let shared_data: &SharedData = shared.inner();
    serde_json::to_string(&shared_data.flights).unwrap()
}
#[get("/bookings")]
fn get_all_bookings(shared: &State<SharedData>) -> String {
    let shared_data: &SharedData = shared.inner();
    serde_json::to_string(&shared_data.bookings).unwrap()
}

#[post("/flights", data="<flight_request>")]
fn post_flight(shared: &State<SharedData>, flight_request: Json<FlightRequest>) -> String {
    let shared_data: &SharedData = shared.inner();
    let mut flight_request = flight_request.into_inner();
    let mut flights = shared_data.flights.lock().unwrap();
    for provider in shared_data.providers.lock().unwrap().iter(){
        if provider.api_key == flight_request.provider_key{
            for flight in flight_request.flights.iter(){
                let mut f = flight.clone();
                f.tenant = provider.code.clone();
                flights.push(f);
            }
            return "Success".to_string();
        }
    }
    "Error".to_string()    
}

#[post("/bookings", data="<booking_request>")]
fn post_booking(shared: &State<SharedData>, booking_request: Json<BookingRequest>) -> String {
    let shared_data: &SharedData = shared.inner();
    let mut booking_request = booking_request.into_inner();
    let mut bookings = shared_data.bookings.lock().unwrap();
    let mut flights = shared_data.flights.lock().unwrap();
    for provider in shared_data.providers.lock().unwrap().iter(){
        if provider.api_key == booking_request.provider_key{
            for ticket in booking_request.booking.tickets.iter(){
                for mut flight in flights.iter_mut(){
                    if flight.internal_code == ticket.flight_code{
                        flight.total_seats -= 1;
                        
                    }
                }
            }
            bookings.push(booking_request.booking.clone());
            return "Success".to_string();
        }
    }
    "Error".to_string()    
}

#[launch]
fn rocket() -> _ {
    rocket::build()
    .manage(SharedData {
        flights: Mutex::new(intialize_flights()),
        providers: Mutex::new(intialize_providers()),
        bookings: Mutex::new(Vec::new())
    })
    .mount("/", routes![index, get_all_flights, post_flight, post_booking, get_all_bookings])
   
   
}

fn intialize_flights() -> Vec<Flight> {
    let mut v = Vec::new();
    v.push(Flight {
        tenant: "TOTO".to_string(),
        departure: Airports::CDG,
        arrival: Airports::DTW,
        internal_code: "code".to_string(),
        available_options: Vec::new(),
        stop_overs: Vec::new(),
        total_seats: 200,
        price: 300
    });
    v
}
fn intialize_providers() -> Vec<FlightProvider> {
    let mut v = Vec::new();
    v.push(FlightProvider {
        code: "GROUP1".to_string(),
        api_key: "8319768420".to_string()
    });
    v.push(FlightProvider {
        code: "GROUP2".to_string(),
        api_key: "2016939750".to_string()
    });
    v.push(FlightProvider {
        code: "GROUP3".to_string(),
        api_key: "8358221978".to_string()
    });
    v.push(FlightProvider {
        code: "GROUP4".to_string(),
        api_key: "0097836438".to_string()
    });
    v.push(FlightProvider {
        code: "GROUP5".to_string(),
        api_key: "7785976847".to_string()
    });
    v.push(FlightProvider {
        code: "GROUP6".to_string(),
        api_key: "8569594981".to_string()
    });
    v.push(FlightProvider {
        code: "GROUP7".to_string(),
        api_key: "3694270696".to_string()
    });
    v
}


